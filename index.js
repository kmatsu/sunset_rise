var app = new Vue({
    el: '#app',
    data: function() {
        return {
          sunset: "",
          sunrise: "",
          latitude: "",
          longitude: "",
          errors: []
      }
    },
    methods: {
      getTimes: function (event) {
        this.errors = [];
        if (this.validLatLong(this.latitude, this.longitude)){
          this.request(this.latitude, this.longitude);
        }
      },
      validLatLong: function (latitude, longitude) {
        if (!this.validLatitude(latitude)) {
          this.errors.push('Latitude must be decimal! from -90 to 90');
        }
        if (!this.validLongitude(longitude)) {
          this.errors.push('Longitude must be decimal! from -180 to 180');
        }
        if (!this.errors.length) {
          return true
        } else {
          return false
        }
      },
      validDecimal: function (str) {
        var re = /^[-]?([1-9]\d*|0)(\.\d+)?$/
        if (str.match(re)) {
          return true
        } else {
          return false
        }
      },
      validRange: function (str, min, max) {
        var num = parseFloat(str);
        if (num >= min && num <= max) {
          return true
        } else {
          return false
        }
      },
      validLatitude: function (str) {
        if (this.validDecimal(str) && this.validRange(str, -90.0, 90.0)) {
          return true
        } else {
          return false
        }
      },
      validLongitude: function (str) {
        if (this.validDecimal(str) && this.validRange(str, -180.0, 180.0)) {
          return true
        } else {
          return false
        }
      },
      request: function (latitude, longitude) {
        var url = 'https://api.sunrise-sunset.org/json?lat=' + latitude + '&lng=' + longitude;
        axios
          .get(url)
          .then(function (res) { 
            // console.log(res.data);           
            app.sunset = res.data.results.sunset;
            app.sunrise = res.data.results.sunrise;
          })
          .catch(function (err) {
            this.errors.push('API response ERROR');
          });
      }
    }
 })